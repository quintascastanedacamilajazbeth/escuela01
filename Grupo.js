function Grupo(c,n){
  
  
  var _tipo="grupo";
  var _clave=c;
  var nombre=n;
  var alumnos= new Array(20);
  var j=0;
  
  function listar(){
     for(var i=0; i<j;i++){
       var cont=alumnos[i];
       console.log(cont.getControl()+", "+cont.getNombre()+" "+cont.getPaterno()+" "+cont.getMaterno()+", "+ cont.getGenero());
     }
  }
  
  function agregar(alumno){
    alumnos[j]=alumno;
    j++;
  } 
  
  function borrar(controlA){
    var buscar;
    for(var p=0;p<=j;p++){
       buscar=alumnos[p];
      if(buscar.getControl()===controlA){
        alumno[p]=alumno[p+1];
      }
    }
  }
  
  function modificar(controlA,con,nom,gen,ap,am){
    var mod;
    for(var p=0;p<=j;p++){
        mod=alumnos[p];
      if(mod.getControl()===controlA){
        mod=alumnos[p];
        mod.setControl(con);
        mod.setNombre(nom);
        mod.setGenero(gen);
        mod.setPaterno(ap);
        mod.setMaterno(am);
        alumnos[p]=mod;
      }
    }
  }
  
  return{
    "tipo":  _tipo,
    "clave": _clave,
    "nombre": nombre,
    "alumnos":alumnos,
    "agregar":agregar,
    "modificar":modificar,
    "borrar":borrar,
    "listar":listar 
  };
  
}