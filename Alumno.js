function Alumno(nc,n,g,ap,am){
  
  
  var _tipo="alumno";
  var nControl=nc;
  var nombre=n;
  var genero=g;
  var aPat=ap;
  var aMat=am;
  
  function getControl(){
    return nControl;
  }
  
  
  function getNombre(){
    return nombre;
  }
  
  function getPaterno(){
    return aPat;
  }
  
  function getMaterno(){
    return aMat;
  }
  
  function getGenero(){
    return genero;
  }
  
  function setControl(newControl){
    nControl=newcontrol;
  }
  
  
  function setNombre(newNombre){
    nombre=newNombre;
  }
  
  function setPaterno(paterno){
    aPat=paterno;
  }
  
  function setMaterno(materno){
    aMat=materno;
  }
  return{
    "tipo":     _tipo,
    "nControl": nControl,
    "nombre":   nombre,
    "genero":   genero,
    "Paterno": aPat,
    "Materno": aMat,
    "getControl":getControl,
    "getNombre":getNombre,
    "getPaterno": getPaterno,
    "getMaterno": getMaterno,
    "getGenero":getGenero,
    "setControl":setControl,
    "setNombre":setNombre,
    "setPaterno": setPaterno,
    "setMaterno": setMaterno,
    
  };
}